#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libsocket/libinetsocket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60


FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s, char fn[]);
void download_all(FILE *s);
void print_progress (double percentage);
void quit(FILE *s);
void error(char *);
int exists(const char *fname);

int main()
{
    FILE * s = connect_to_server();
    // Menu
    menu();
    
    // Get choice
    char choice = get_choice();
    
    // Handle choice
    switch(choice)
    {
        case 'l':
        case 'L':
            list_files(s);
            break;
        
        case 'd':
        case 'D':
            download(s, "");
            break;
            
        case 'a':
        case 'A':
            printf("Download all selected.\n");
            download_all(s);
            break;
                
        case 'q':
        case 'Q':
            quit(s);
            exit(0);
            break;
            
        default:
            printf("Choice must be l, d, a, or q\n");
    }
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{
    // Connect socket
    int sockfd = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    if (!sockfd)
    {
        error("Can't connect to server");
    }

    FILE *s = fdopen(sockfd, "r+");
    
    return s;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("L) List files\n");
    printf("D) Download a file\n");
    printf("A) Download all files\n");
    printf("Q) Quit\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    char buf[1000];
    int size = 0;
    char filename[100] = "";
    fprintf(s, "LIST\n");
    while (fgets(buf, 1000, s) != NULL)
    {
        sscanf(buf, "%d %s", &size, filename);
        if (!strcmp(filename, "") == 0)
        {
            if(strcmp(buf, ".\n") == 0)
            {
                break;
            } else
            {
                printf("%s\n", filename);
            }
        }
    }
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s, char fn[])
{
    char buf[1000];
    char filename[100];
    long size;
    int not_empty_filename = strcmp(fn, "");
    if (not_empty_filename == 0)
    {
        list_files(s);
        printf("Type a filename to download: ");
        scanf("%s", filename);
    }
    else
    {
        strcpy(filename, fn);
        
        
    }
    printf("%s\n", filename);
    //printf("After if/else.");
    char overwrite[100];
    if (exists(filename) == 1)
    {
        printf("File already exists. Rewrite file? (y/n): ");
        scanf("%s", overwrite);
        //printf("Overwrite: %s\n", overwrite);
    }
    else
    {
        strcpy(overwrite, "y");
    }
    //printf("SIZE %s\n", filename);
    //printf("%s", overwrite);
    if (strcmp(overwrite, "y") == 0)
    {
        int size_check = 0;
        fprintf(s, "SIZE %s\n", filename);
        
        //When using download all, this block does not run for some reason.
        //printf("%s", fgets(buf, 1000, s));
        while (fgets(buf, 1000, s) != NULL)
        {
            //printf("buf: %s\n", buf);
            if (size_check == 0)
            {
                size = atoi(buf+4);
                break;
            }
        }
        //printf("Size: %ld\n", size);
        fprintf(s, "GET %s\n", filename);
        printf("GET %s\n", filename);
        FILE *fp;
        fp = fopen(filename, "w+");
        long so_far = 0;
        long got;
        int want;
        fgets(buf, 1000, s);
        double percentage_done;
        while (1)
        {
            if (size - so_far < 100)
            {
                want = size - so_far;
            }
            else
            {
                want = 100;
            }
            got = fread(buf, sizeof(unsigned char), want, s);
            //fprintf(stderr, "So_far: %d\n", so_far);
            fwrite(buf, sizeof(unsigned char), got, fp);
            so_far += got;
            percentage_done = (double) so_far/size;
            //printf("before print progress.\n\n\n");
            print_progress(percentage_done);
            
            if (so_far >= size)
            { 
                printf("\n");
                break;
            }
        }
        fclose(fp);
    }
    else
    {
        printf("File already exists. File will remain.\n");    
    }
}

void download_all(FILE *s)
{
    char buf[1000];
    int size = 0;
    char fn[100] = "";
    fprintf(s, "LIST\n");
    fgets(buf, 1000, s);
    
    while (fgets(buf, 1000, s) != NULL)
    {
        sscanf(buf, "%d %s", &size, fn);
        if (!strcmp(fn, "") == 0)
        {
            if(strcmp(buf, ".\n") == 0)
            {
                break;
            } else
            {
                //printf("filename download_all: %s\n", fn);
                //printf("Entering download stage %s.", buf);
                download(s, fn);
            }
        }
    }
}

/* 
 * Close the connection to the server.
 */
void quit(FILE *s)
{
    fclose(s);
}

void error(char *s)
{
    fprintf(stderr, "%s\n", s);
    exit(1);
}

void print_progress (double percentage)
{
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}

int exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}