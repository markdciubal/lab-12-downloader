all: download

# This rule links hashpass.o and md5.o along with the
# libssl and libcrypto libraries to make the executable.
download: download.o
	clang -g download.c -o download -l socket
	
clean:
	rm -f *.txt *.jpg *.exe *.raw *.mp3